package br.com.ptqs.web.exemplo.pages;

import br.com.ptqs.web.configuration.ExemploTestRules;
import br.com.ptqs.web.datahelper.FuncionarioDTO;
import br.com.ptqs.web.utils.ExemploDriverUtils;
import org.junit.Assert;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;

public class ExemploAreaLogadaPage extends ExemploAreaLogadaMap {

	public ExemploAreaLogadaPage() {
		PageFactory.initElements(ExemploTestRules.getDriver(), this);
	}
	
	// Ações da Página
	
	public void verificarLoginComSucesso() throws IOException {
	
		ExemploDriverUtils exemploDriverUtils = new ExemploDriverUtils();
		
		exemploDriverUtils.waitVisibility(menuLinkLogoSite);
		exemploDriverUtils.waitVisibility(menuLinkFuncionarios);
		exemploDriverUtils.waitVisibility(menuLinkNovoFuncionario);
		exemploDriverUtils.waitVisibility(menuLinkSair);
		exemploDriverUtils.waitVisibility(tableInputSearch);
		exemploDriverUtils.waitVisibility(tableTableEntries);
		
	}
	
	public void verificarTelaInicialExibidaComSucesso() throws IOException {

		String stringMenu;

		stringMenu = menuLinkSair.getText();
		Boolean loginSucesso = stringMenu.equalsIgnoreCase("sair");

		Assert.assertTrue("Funcionário exibido com sucesso na listagem", loginSucesso);

	}
	
	public void acessarFuncionalidadeCadastrarUsuario() {
		
		menuLinkNovoFuncionario.click();
		
	}

	public void verificarCadastroRealizadoSucesso() throws IOException {
		
		ExemploDriverUtils exemploDriverUtils = new ExemploDriverUtils();
		
		exemploDriverUtils.waitVisibility(alertSuccess);
		Boolean funcionarioCadastrado = alertSuccess.isDisplayed();

		Assert.assertTrue("Funcionário exibido com sucesso na listagem", funcionarioCadastrado);
		
	}

	public void verificarFuncinarioNaListagem(FuncionarioDTO funcionarioDTO) throws IOException {
		
		ExemploDriverUtils exemploDriverUtils = new ExemploDriverUtils();
		
		exemploDriverUtils.waitVisibility(tableInputSearch);
		tableInputSearch.sendKeys(funcionarioDTO.getNome());
		
		if (funcionarioDTO.getSexo() == "Indiferente") {
			funcionarioDTO.setSexo("Indefinido");
		}
		
		String funcionario = funcionarioDTO.getNome()
				.concat(" " + funcionarioDTO.getCpf())
				.concat(" " + funcionarioDTO.getSexo())
				.concat(" " + funcionarioDTO.getCargo())
				.concat(" " + funcionarioDTO.getAdmissao());
		
		String registros = tableTableEntries.getText();
		Boolean funcionarioExiste = registros.contains(funcionario);

		Assert.assertTrue("Funcionário exibido com sucesso na listagem", funcionarioExiste);

	}
	
}
