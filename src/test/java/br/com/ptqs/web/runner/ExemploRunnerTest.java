package br.com.ptqs.web.runner;

import br.com.ptqs.web.exemplo.test.ExemploCadastroSuite;
import br.com.ptqs.web.exemplo.test.ExemploLoginSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
		ExemploLoginSuite.class,
		ExemploCadastroSuite.class
})
public class ExemploRunnerTest {
}
