package br.com.ptqs.web.exemplo.test;

import br.com.ptqs.web.configuration.ExemploTestRules;
import br.com.ptqs.web.exemplo.pages.ExemploAreaLogadaPage;
import br.com.ptqs.web.exemplo.pages.ExemploLoginPage;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import java.io.IOException;

public class ExemploLoginSuite extends ExemploTestRules {

	@Rule public TestName name = new TestName();

	@Test
	public void testDeveRealizarLoginComSucesso() throws IOException {
		ExemploLoginPage exemploLoginPage = new ExemploLoginPage();

		exemploLoginPage.validarElementosExemploPageLogin();
		exemploLoginPage.preencherCampoUsername("test_user");
		exemploLoginPage.preencherCampoPassword("test123!");
		exemploLoginPage.acionarBotaoEntre();

		ExemploAreaLogadaPage exemploAreaLogadaPage = new ExemploAreaLogadaPage();

		exemploAreaLogadaPage.verificarLoginComSucesso();
		exemploAreaLogadaPage.verificarTelaInicialExibidaComSucesso();
	}

	@Test
	public void testDeveRealizarLoginSemSucesso() throws IOException {
		ExemploLoginPage exemploLoginPage = new ExemploLoginPage();

		exemploLoginPage.validarElementosExemploPageLogin();
		exemploLoginPage.preencherCampoUsername("test_user");
		exemploLoginPage.preencherCampoPassword("test123!_invalida");
		exemploLoginPage.acionarBotaoEntre();

		exemploLoginPage.verificarLoginSemSucesso();
		exemploLoginPage.verificarLoginApresentaMensagemDeErro();
	}
	
}
