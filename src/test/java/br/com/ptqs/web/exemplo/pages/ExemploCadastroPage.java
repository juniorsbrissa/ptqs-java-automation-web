package br.com.ptqs.web.exemplo.pages;

import br.com.ptqs.web.configuration.ExemploTestRules;
import br.com.ptqs.web.datahelper.FuncionarioDTO;
import br.com.ptqs.web.utils.ExemploDriverUtils;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.io.IOException;

public class ExemploCadastroPage extends ExemploCadastroMap {

	public ExemploCadastroPage() {
		PageFactory.initElements(ExemploTestRules.getDriver(), this);
	}
	
	// Ações da Página
	
	public void verificarFormularioDeCadastro() throws IOException {
		
		ExemploDriverUtils exemploDriverUtils = new ExemploDriverUtils();
		
		exemploDriverUtils.waitVisibility(inputNome);
		exemploDriverUtils.waitVisibility(inputCpf);
		exemploDriverUtils.waitVisibility(selectSexo);
		exemploDriverUtils.waitVisibility(inputAdmissao);
		exemploDriverUtils.waitVisibility(inputCargo);
		exemploDriverUtils.waitVisibility(inputSalario);
		exemploDriverUtils.waitVisibility(inputClt);
		exemploDriverUtils.waitVisibility(inputPj);
		exemploDriverUtils.waitVisibility(inputEnviar);
		exemploDriverUtils.waitVisibility(inputCancelar);
		
	}
	
	public void cadastrarFuncionario(FuncionarioDTO funcionarioDTO) throws IOException {
		
		inputNome.sendKeys(funcionarioDTO.getNome());
		inputCpf.sendKeys(funcionarioDTO.getCpf());
		inputAdmissao.sendKeys(funcionarioDTO.getAdmissao());
		inputCargo.sendKeys(funcionarioDTO.getCargo());
		inputSalario.sendKeys(funcionarioDTO.getSalario());
		
		Select dropdownSexo = new Select(selectSexo);
		dropdownSexo.selectByVisibleText(funcionarioDTO.getSexo());
		
		if (funcionarioDTO.getClt()) {
			inputClt.click();
		} else {
			inputPj.click();
		}
		
		inputEnviar.click();
		
	}
	
}
