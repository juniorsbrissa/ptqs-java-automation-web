package br.com.ptqs.web.exemplo.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ExemploCadastroMap {
	
	// Elementos da Página
	
	@FindBy(xpath = "//input[@name='nome']")
	protected WebElement inputNome;
	
	@FindBy(xpath = "//input[@name='cpf']")
	protected WebElement inputCpf;
	
	@FindBy(xpath = "//select[@name='sexo']")
	protected WebElement selectSexo;
	
	@FindBy(xpath = "//input[@name='admissao']")
	protected WebElement inputAdmissao;
	
	@FindBy(xpath = "//input[@name='cargo']")
	protected WebElement inputCargo;
	
	@FindBy(xpath = "//input[@name='salario']")
	protected WebElement inputSalario;
	
	@FindBy(xpath = "//input[@id='clt']")
	protected WebElement inputClt;
	
	@FindBy(xpath = "//input[@id='pj']")
	protected WebElement inputPj;
	
	@FindBy(xpath = "//input[@type='submit']")
	protected WebElement inputEnviar;
	
	@FindBy(xpath = "//input[@type='reset']")
	protected WebElement inputCancelar;

}
