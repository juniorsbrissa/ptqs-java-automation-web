package br.com.ptqs.web.exemplo.test;

import br.com.ptqs.web.configuration.ExemploTestRules;
import br.com.ptqs.web.datahelper.FuncionarioDTO;
import br.com.ptqs.web.exemplo.pages.ExemploAreaLogadaPage;
import br.com.ptqs.web.exemplo.pages.ExemploCadastroPage;
import br.com.ptqs.web.exemplo.pages.ExemploLoginPage;
import org.junit.Before;
import org.junit.Test;

public class ExemploCadastroSuite extends ExemploTestRules {
	
	FuncionarioDTO funcionarioDTO = new FuncionarioDTO();

	@Before
	public void setUp() throws Exception {
		ExemploLoginPage exemploLoginPage = new ExemploLoginPage();
		exemploLoginPage.validarElementosExemploPageLogin();
		exemploLoginPage.preencherCampoUsername("test_user");
		exemploLoginPage.preencherCampoPassword("test123!");
		exemploLoginPage.acionarBotaoEntre();

		ExemploAreaLogadaPage exemploAreaLogadaPage = new ExemploAreaLogadaPage();
		exemploAreaLogadaPage.verificarLoginComSucesso();
	}
	
	@Test
	public void testDeveCadastrarFuncionarioComSucesso() throws Exception {
		ExemploAreaLogadaPage exemploAreaLogadaPage = new ExemploAreaLogadaPage();
		exemploAreaLogadaPage.acessarFuncionalidadeCadastrarUsuario();

		funcionarioDTO.initializeFuncionarioDTO();

		ExemploCadastroPage exemploCadastroPage = new ExemploCadastroPage();
		exemploCadastroPage.verificarFormularioDeCadastro();
		exemploCadastroPage.cadastrarFuncionario(funcionarioDTO);

		exemploAreaLogadaPage.verificarCadastroRealizadoSucesso();
		exemploAreaLogadaPage.verificarFuncinarioNaListagem(funcionarioDTO);
	}
	
}
