package br.com.ptqs.web.exemplo.pages;

import br.com.ptqs.web.configuration.ExemploTestRules;
import br.com.ptqs.web.utils.ExemploDriverUtils;
import org.junit.Assert;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;

public class ExemploLoginPage extends ExemploLoginMap {

	public ExemploLoginPage() {
		PageFactory.initElements(ExemploTestRules.getDriver(), this);
	}
	
	// Ações da Página
	
	public void validarElementosExemploPageLogin() throws IOException {
		
		ExemploDriverUtils exemploDriverUtils = new ExemploDriverUtils();
		
		exemploDriverUtils.waitVisibility(menuLinkLogoSite);
		exemploDriverUtils.waitVisibility(menuLinkCadastreSe);
		exemploDriverUtils.waitVisibility(menuLinkLogin);
		exemploDriverUtils.waitVisibility(formHeaderLogin);
		exemploDriverUtils.waitVisibility(formInputUsername);
		exemploDriverUtils.waitVisibility(formInputPassword);
		exemploDriverUtils.waitVisibility(formButtonEntre);
		exemploDriverUtils.waitVisibility(formLinkCadastreSe);
		
	}
	
	public void preencherCampoUsername(String username) {
		
		formInputUsername.sendKeys(username);
		
	}
	
	public void preencherCampoPassword(String password) throws IOException {
		
		formInputPassword.sendKeys(password);
		
	}
	
	public void acionarBotaoEntre() {
		
		formButtonEntre.click();
		
	}
	
	public void verificarLoginSemSucesso() throws IOException {
		ExemploDriverUtils exemploDriverUtils = new ExemploDriverUtils();
		
		exemploDriverUtils.waitVisibility(alertMessageErro);
		Boolean falhaLogin = alertMessageErro.isDisplayed();

		Assert.assertTrue("Login não efetuado", falhaLogin);
		
	}
	
	public void verificarLoginApresentaMensagemDeErro() throws IOException {
		
		String stringMensagemErro;
		
		stringMensagemErro = alertMessageErro.getText();
		Boolean mensagem = stringMensagemErro.contains("ERRO! Usuário ou Senha inválidos");

		Assert.assertTrue("Mensagem de erro exibida com sucesso", mensagem);
		
	}
	
}
