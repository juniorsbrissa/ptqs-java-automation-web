package br.com.ptqs.web.configuration;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class ExemploTestRules {
	
	private static WebDriver driver;

	public static WebDriver getDriver() {
		return driver;
	}

	
	@Before // Set Up
	public void beforeScenario() {

		try {
			WebDriverManager.chromedriver().setup();

			ChromeOptions options = new ChromeOptions();
			options.addArguments("--window-size=1920,1080");
			options.addArguments("--disable-dev-shm-usage");
			options.addArguments("--headless");
			
			driver = new ChromeDriver(options);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.navigate().to("http://www.myrobo.tk");
		} catch (Exception e) {
			Assert.fail();
		}
		
	}
	
	@After // Tear Down
	public void afterScenario() {
		
		try {
			if (driver != null) {
				driver.close();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}

	}
	
}
